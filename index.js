	const fullname = "Steve Rogers";
	console.log(fullname);

	let age = 40;
	console.log(age);
	
	let friends = ["Tony","Bruce", "Thor", "Natasha" ,"Clint","Nick"];
	console.log(friends);

	let profile = {

		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false
	}

	console.log(profile);

	let fullname1 = "Tony Stark";
	console.log(fullname1);


	let largestOcean = "Atlantic Ocean";
	console.log(largestOcean);

	//	function for getting the sum of two numbers

	function getSum(num1, num2){
		/*
			let sum = num1 + num2;

			console.log(sum);
		*/

		return num1 + num2;
	};

	// sumNum(3, 5);

	let total = getSum(10, 55);
	console.log(total);